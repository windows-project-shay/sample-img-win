FROM mcr.microsoft.com/dotnet/framework/sdk:4.8


SHELL [ "powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';" ]

ARG chocolateyUseWindowsCompression='false'
ARG REGISTRATION_TOKEN=''
ARG CI_SERVER_URL='https://gitlab.lfg.com/'
ARG RUNNER_TAG_LIST=''
ARG RUNNER_NAME=''
ARG RUNNER_EXECUTOR='shell'
ARG RUNNER_SHELL='powershell'
ARG CONFIG_FILE=''
ARG REGISTER_RUN_UNTAGGED='false'
ARG RUNNER_REQUEST_CONCURRENCY='4'
ARG RUNNER_BUILDS_DIR=''
ARG RUNNER_CACHE_DIR=''
ARG AUTO_UPDATE='true'

ENV chocolateyUseWindowsCompression ${chocolateyUseWindowsCompression}
ENV REGISTRATION_TOKEN ${REGISTRATION_TOKEN}
ENV CI_SERVER_URL ${CI_SERVER_URL}
ENV RUNNER_TAG_LIST ${RUNNER_TAG_LIST}
ENV RUNNER_NAME ${RUNNER_NAME}
ENV RUNNER_EXECUTOR ${RUNNER_EXECUTOR}
ENV RUNNER_SHELL ${RUNNER_SHELL}
ENV CONFIG_FILE ${CONFIG_FILE}
ENV REGISTER_RUN_UNTAGGED ${REGISTER_RUN_UNTAGGED}
ENV RUNNER_REQUEST_CONCURRENCY ${RUNNER_REQUEST_CONCURRENCY}
ENV RUNNER_BUILDS_DIR ${RUNNER_BUILDS_DIR}
ENV RUNNER_CACHE_DIR ${RUNNER_CACHE_DIR}
ENV AUTO_UPDATE ${AUTO_UPDATE}

ADD https://chocolatey.org/api/v2/package/chocolatey c:/packages/chocolatey.nupkg
COPY install-choco.ps1 c:/ 

# install chocolatey
RUN & Set-ExecutionPolicy Bypass -Scope Process -Force; \
      Invoke-Expression -Command ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')); 

RUN choco install --no-progress -y git -params "/GitAndUnixToolsOnPath"; \
mkdir c:\gitlab-runner; \
choco install gitlab-runner --no-progress -y  -params "/InstallDir:c:\\gitlab-runner"; \
Install-PackageProvider -Name NuGet -Force; \
Register-PSRepository -verbose -name 'LFGallery' -SourceLocation 'https://artifactory.jfrog.lfg.com/artifactory/api/nuget/edevops-powershell-gallery' -PublishLocation 'https://artifactory.jfrog.lfg.com/artifactory/api/nuget/edevops-powershell-gallery' -PackageManagementProvider  'nuget' -InstallationPolicy 'Trusted'; \
#remove to PSGallery as the LFGallery has it set as a remote
Unregister-PSRepository -verbose -Name PSGallery; \
#install modules - we need LFG.Deploy but first are requirements of the Deploy module
# aws ecs deployment do not leverage XLR in any way.
Install-Module -verbose -name LFG.Web -Scope AllUsers -force; \
Install-Module -verbose -name LFG.XLRelease -Scope AllUsers -force; \
Install-Module -verbose -name LFG.Gitlab -Scope AllUsers -force; \
Install-Module -verbose -name LFG.DBMaestro -Scope AllUsers -force; \
Install-Module -verbose -name LFG.Deploy -Scope AllUsers -force;


ADD root/ / 

CMD [ "powershell", "-File", "entrypoint.ps1" ]
